<?php

require_once("Model/User.php");
require_once("Entity/Staff.php");
$user = new User();
$staff = new Staff();
$staff->setCode($_POST['code']);
$staff->setUsername($_POST['username']);
$staff->setPassword(password_hash($_POST['password'], PASSWORD_DEFAULT));
$staff->setFirstname($_POST['firstname']);
$staff->setLastname($_POST['lastname']);
$staff->setGender($_POST['gender']);
$staff->setBirthday($_POST['birthday']);
$staff->setPhone($_POST['phone']);
$staff->setAddress($_POST['address']);
$staff->setDivision($_POST['division']);
if ($user->insertUser($staff)) {

    echo json_encode(array('status' => 1));
} else {
    echo json_encode(array('status' => 0, 'message' => 'Update fail'));
}
?>
