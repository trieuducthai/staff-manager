<?php
require_once("Model/User.php");
$oldcode = $_POST['oldcode'];
$code = $_POST['code'];
$user = new User();
$arr = $user->getCode();

if ($code === $oldcode){
    echo json_encode(array('status' => 1));
}
else {
    $check = true;
    $i = 0;
    while ($i < count($arr)) {
        if ($code === $arr[$i]->getCode()){
            $check = false;
            break;
        }
        $i++;
    }
    if ($check === true) {
        echo json_encode(array('status' => 1));
    }
    else {
        echo json_encode(array('status' => 0));
    }
}
?>

