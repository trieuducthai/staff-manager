<?php

class Staff {

    private $_code;
    private $_username;
    private $_password;
    private $_firstname;
    private $_lastname;
    private $_gender;
    private $_birthday;
    private $_phone;
    private $_address;
    private $_division;
    
    function Staff(){
        
    }

    // GET and SET ID
    public function getCode() {
        return $this->_code;
    }

    public function setCode($code) {
        $this->_code = $code;
    }

    // GET and SET Username
    public function getUsername() {
        return $this->_username;
    }

    public function setUsername($username) {
        $this->_username = $username;
    }

    // GET and SET Password
    public function getPassword() {
        return $this->_password;
    }

    public function setPassword($password) {
        $this->_password = $password;
    }

    // GET and SET Firstname
    public function getFirstname() {
        return $this->_firstname;
    }

    public function setFirstname($firstname) {
        $this->_firstname = $firstname;
    }

    // GET and SET Lastname
    public function getLastname() {
        return $this->_lastname;
    }

    public function setLastname($lastname) {
        $this->_lastname = $lastname;
    }

    // GET and SET Gender
    public function getGender() {
        return $this->_gender;
    }

    public function setGender($gender) {
        $this->_gender = $gender;
    }

    // GET and SET Birthday
    public function getBirthday() {
        return $this->_birthday;
    }

    public function setBirthday($birthday) {
        $this->_birthday = $birthday;
    }

    // GET and SET Phone
    public function getPhone() {
        return $this->_phone;
    }

    public function setPhone($phone) {
        $this->_phone = $phone;
    }

    // GET and SET Address
    public function getAddress() {
        return $this->_address;
    }

    public function setAddress($address) {
        $this->_address = $address;
    }

    // GET and SET Division
    public function getDivision() {
        return $this->_division;
    }

    public function setDivision($division) {
        $this->_division = $division;
    }

//    // GET and SET Position
//    public function getPosition() {
//        return $this->_position;
//    }
//
//    public function setPosition($position) {
//        $this->_position = $position;
//    }
//
//    // GET and SET Salary
//    public function getSalary() {
//        return $this->_salary;
//    }
//
//    public function setSalary($salary) {
//        $this->_salary = $salary;
//    }
}
?>