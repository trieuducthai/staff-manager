<?php

require_once("Model/User.php");
$code = $_POST['usercode'];
$user = new User();
$staff = $user->getRowbyCode($code);
echo json_encode(array('code' => $staff->getCode(), 'firstname' => $staff->getFirstname(),
    'lastname' => $staff->getLastname(), 'username' => $staff->getUsername(),
    'password' => $staff->getPassword(), 'gender' => $staff->getGender(),
    'birthday' => $staff->getBirthday(), 'phone' => $staff->getPhone(),
    'address' => $staff->getAddress(), 'division' => $staff->getDivision()));
?>