<?php

require_once("Model/User.php");
require_once("Lib/Paging.php");
$code = $_POST['code'];
$user = new User();
$staff = new Staff();

if ($user->deleteUser($code)) {

    echo json_encode(array('status' => 1));
} else {
    echo json_encode(array('status' => 0, 'message' => 'Delete fail'));
}
?>