<?php
session_start();
include("inc/header.php");
require("Model/User.php");
require_once("Lib/Paging.php");
?>
<div id="main" class="container">
    <h2>Staff manager</h2>
    <div class="main-header">
        <div class="user-login">
            <?php 
                if (isset($_SESSION['name']))
                {
                    echo "Hello <b>".$_SESSION["name"]."</b>!  ";
                    echo "<a id='log-out' href='#'>Log out</a>";
                }
            ?>
        </div>
        <div class="pagger">
            <?php
            $data = new User();
            if (isset($_POST["txt-search"])){
                $value = $_POST["txt-search"];
                $keyword = array("code", "firstname", "lastname");
                $paging = new Paging($keyword, $value);
                $arr = $data->getPager($paging->start, $paging->limit, $keyword, $value);
            } else {
                $keyword = array();
                $paging = new Paging($keyword, "");
                $arr = $data->getPager($paging->start, $paging->limit, $keyword, "");
            }
            echo $paging->pagesList($paging->curpage);
            ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="table-view">
        <table id="table-staff" cellspacing="0">
            <tr class="search-row">
                <td colspan="9">
                <form method="post">
                    <input type="text" name="txt-search" id="txt-search" placeholder="Search for staff code or name..." 
                           value="<?php echo isset($_POST['txt-search'])?$_POST['txt-search']:''; ?>">
                    <input type="submit" name="btn-search" id="btn-search" value="Search">
                </form>
                </td>
            </tr>
            <tr class="header-row">
                <td>Staff code</td>
                <td>Last name</td>
                <td>First name</td>
                <td>Gender</td>
                <td>Birthday</td>
                <td>Phone</td>
                <td>Address</td>
                <td>Division</td>
                <td>Action</td>
            </tr>
            <div id="tr-update">
                <?php
                $i = 0;
                while ($i < count($arr)) {
                    if ($i % 2 == 0) {
                        echo "<tr id = 'user" . $i . "' style='background-color:#e7e3e3'>";
                    } else {
                        echo "<tr id = 'user" . $i . "' style='background-color:#f1eded'>";
                    }
                    echo "<td>" . $arr[$i]->getCode() . "</td>";
                    echo "<td>" . $arr[$i]->getLastname() . "</td>";
                    echo "<td>" . $arr[$i]->getFirstname() . "</td>";
                    echo "<td>" . $arr[$i]->getGender() . "</td>";
                    echo "<td>" . $arr[$i]->getBirthday() . "</td>";
                    echo "<td>" . $arr[$i]->getPhone() . "</td>";
                    echo "<td>" . $arr[$i]->getAddress() . "</td>";
                    echo "<td>" . $arr[$i]->getDivision() . "</td>";
                    echo "<td><a class='edit' name='edit' href='#' data-code='" . $arr[$i]->getCode() . "'>Edit</a>"
                    . "<a class='delete' name='delete' href='#' data-code='" . $arr[$i]->getCode() . "'>Delete</a></td>";
                    echo "</tr>";
                    $i++;
                }
                ?>
            </div>
            <tr class="footer-row">
                <td colspan="2"><a href="#" id="insert">Add new...</a></td>
            </tr>
        </table>
    </div>
    <div id="addnew-main">
        <div class="add-new">
            <a class="addnew-close">x</a>
            <h2>Add New</h2>
            <form id="add-form" method="post">
                <label>Staff code:</label>
                <div class="add-input">
                    <input type="text" class="txt-code" id="txt-newcode" name="txt-newcode" placeholder="Staff code">
                    <p id="alert-newcode"></p>
                </div>
                <label>Username:</label>
                <div class="add-input">
                    <input type="text" class="txt-username" id="txt-newusername" name="txt-newusername" placeholder="User name">
                    <p id="alert-newusername"><p>
                </div>
                <label>Password:</label>
                <div class="add-input">
                    <input type="password" class="txt-password" id="txt-newpassword" name="txt-newpassword" placeholder="Password">
                    <p id="alert-newpassword"></p>
                </div>
                <label>First name:</label>
                <div class="add-input">
                    <input type="text" class="txt-firstname" id="txt-newfirstname" name="txt-newfirstname" placeholder="First name">
                    <p id="alert-newfirstname"><p>
                </div>
                <label>Last name:</label>
                <div class="add-input">
                    <input type="text" class="txt-lastname" id="txt-newlastname" name="txt-newlastname" placeholder="Last name">
                    <p id="alert-newlastname"></p>
                </div>
                <label>Gender:</label>
                <div class="add-input">
                    <div class="gender-input">
                        <input type="radio" id="rdo-newfemale" name="rdo-newgender" value="0" checked>
                        <label for="rdo-newfemale">Female</label>
                    </div>
                    <div class="gender-input">
                        <input type="radio" id="rdo-newmale" name="rdo-newgender" value="1">
                        <label for="rdo-newmale">Male</label>
                    </div>
                    <div class="gender-input">
                        <input type="radio" id="rdo-newother" name="rdo-newgender" value="2">
                        <label for="rdo-newother">Other</label>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <label>Birthday:</label>
                <div class="add-input">
                    <input type="text" class="txt-birthday" id="txt-newbirthday" name="txt-newbirthday" placeholder="YYYY-MM-DD">
                    <p id="alert-newbirthday"></p>
                </div>
                <label>Phone number:</label>
                <div class="add-input">
                    <input type="text" class="txt-phone" id="txt-newphone" name="txt-newphone" placeholder="Phone number">
                    <p id="alert-newphone"></p>
                </div>
                <label>Address:</label>
                <div class="add-input">
                    <input type="text" class="txt-address" id="txt-newaddress" name="txt-newaddress" placeholder="Address">
                    <p id="alert-newaddress"></p>
                </div>
                <label>Division:</label>
                <div class="add-input">
                    <input type="text" class="txt-division" id="txt-newdivision" name="txt-newdivision" placeholder="Division">
                    <p id="alert-newdivision"></p>
                </div>
                <div class="add-input">
                    <input type="button" id="btn-addnew" name="btn-addnew" value="Add">
                </div>
            </form>
        </div>
    </div>
    <div id="update-main">
        <div class="update">
            <a class="update-close">x</a>
            <h2>Update</h2>
            <form id="update-form" method="post">
                <label>Staff code:</label>
                <div class="update-input">
                    <input type="text" class="txt-code" id="txt-editcode" name="txt-editcode" placeholder="Staff code">
                    <p id="alert-editcode"></p>
                </div>
                <label>Username:</label>
                <div class="update-input">
                    <input type="text" class="txt-username" id="txt-editusername" name="txt-editusername" placeholder="Username">
                    <p id="alert-editusername"><p>
                </div>
                <label>Password:</label>
                <div class="update-input">
                    <input type="password" class="txt-password" id="txt-editpassword" name="txt-editpassword" placeholder="Password">
                    <p id="alert-editpassword"></p>
                </div>
                <label>First name:</label>
                <div class="update-input">
                    <input type="text" class="txt-firstname" id="txt-editfirstname" name="txt-editfirstname" placeholder="Firstname">
                    <p id="alert-editfirstname"><p>
                </div>
                <label>Last name:</label>
                <div class="update-input">
                    <input type="text" class="txt-lastname" id="txt-editlastname" name="txt-editlastname" placeholder="Lastname">
                    <p id="alert-editlastname"></p>
                </div>
                <label>Gender:</label>
                <div class="update-input">
                    <div class="gender-input">
                        <input type="radio" id="rdo-editfemale" name="rdo-gender" value="0">
                        <label for="rdo-editfemale">Female</label>
                    </div>
                    <div class="gender-input">
                        <input type="radio" id="rdo-editmale" name="rdo-gender" value="1">
                        <label for="rdo-male">Male</label>
                    </div>
                    <div class="gender-input">
                        <input type="radio" id="rdo-editother" name="rdo-gender" value="2">
                        <label for="rdo-editother">Other</label>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <label>Birthday:</label>
                <div class="update-input">
                    <input type="text" class="txt-birthday" id="txt-editbirthday" name="txt-editbirthday" placeholder="Birthday">
                    <p id="alert-editbirthday"></p>
                </div>
                <label>Phone number:</label>
                <div class="update-input">
                    <input type="text" class="txt-phone" id="txt-editphone" name="txt-editphone" placeholder="Phone number">
                    <p id="alert-editphone"></p>
                </div>
                <label>Address:</label>
                <div class="update-input">
                    <input type="text" class="txt-address" id="txt-editaddress" name="txt-editaddress" placeholder="Address">
                    <p id="alert-editaddress"></p>
                </div>
                <label>Division:</label>
                <div class="update-input">
                    <input type="text" class="txt-division" id="txt-editdivision" name="txt-editdivision" placeholder="Division">
                    <p id="alert-editdivision"></p>
                </div>
                <div class="update-input">
                    <input type="button" id="btn-update" name="btn-update" value="Update">
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>