<?php

require_once 'Model/User.php';

class Paging {

    protected $_total;
    protected $_pages;
    public $limit = 10;
    public $start;
    public $curpage;

    public function Paging($keyword, $value) {
        $this->findTotal($keyword, $value);
        // Tổng số trang
        $this->findPages($this->limit);
        // Bắt đầu từ mẫu tin
        $this->start = $this->rowStart($this->limit);
        // Trang hiện tại
        $this->curpage = ($this->start / $this->limit) + 1;
    }

// Phương thức tìm tổng số mẫu tin
    public function findTotal($keyword, $value) {
            $user = new User();
            $arr = $user->getSearch($keyword, $value);
            $this->_total = count($arr);
    }

// Phương thức tính số trang
    public function findPages($limit) {
        $this->_pages = ceil($this->_total / $limit);
    }

// Phương thức tính vị trí mẫu tin bắt đầu từ vị trí trang
    function rowStart($limit) {
        return (!isset($_GET['page'])) ? 0 : ($_GET['page'] - 1) * $limit;
    }

    public function pagesList($curpage) {
        $total = $this->_total;
        $pages = $this->_pages;
        if ($pages <= 1) {
            return '';
        }
        $page_list = "";

        // Tạo liên kết tới trang đầu và trang trang trước
        if ($curpage != 1) {
            $page_list .= '<a href="' . $_SERVER['PHP_SELF'] . '?page=1&total=' . $total . '" title="trang đầu">First </a>';
        }
        if ($curpage > 1) {
            $page_list .= '<a href="' . $_SERVER['PHP_SELF'] . '?page=' . ($curpage - 1) . '&total=' . $total . '" title="trang trước">< </a>';
        }

        // Tạo liên kết tới các trang
        for ($i = 1; $i <= $pages; $i++) {
            if ($i == $curpage) {
                $page_list .= "<b>" . $i . "</b>";
            } else {
                $page_list .= '<a href="' . $_SERVER['PHP_SELF'] . '?page=' . $i . '&total=' . $total . '" title="Trang ' . $i . '">' . $i . '</a>';
            }
            $page_list .= " ";
        }

        // Tạo liên kết tới trang sau và trang cuối
        if (($curpage + 1) <= $pages) {
            $page_list .= '<a href="' . $_SERVER['PHP_SELF'] . '?page=' . ($curpage + 1) . '&total=' . $total . '" title="Đến trang sau"> > </a>';
        }
        if (($curpage != $pages) && ($pages != 0)) {
            $page_list .= '<a href="' . $_SERVER['PHP_SELF'] . '?page=' . $pages . '&total=' . $total . '" title="trang cuối"> Last</a>';
        }
        return $page_list;
    }

}
?>