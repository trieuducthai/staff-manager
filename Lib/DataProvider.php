<?php

class DataProvider {

    private $_conn;
    private $_database = 'db_staff';

    // Create connection
    function createConnect() {
        $this->_conn = mysqli_connect('localhost', 'root', '',$this->_database) or die('Connect fail');
    }

    // SQL to add a record
    function insert($table, $data) {
        $this->createConnect();
        $field_list = '';
        $value_list = '';
        foreach ($data as $key => $value) {
            $field_list .= ",$key";
            $value_list .= ",'" . mysql_escape_string($value) . "'";
        }
        $sql = 'INSERT INTO ' . $table . '(' . trim($field_list, ',') . ') VALUES (' . trim($value_list, ',') . ')';
        return mysqli_query($this->_conn, $sql);
    }

    // SQL to delete a record
    function delete($table, $where) {
        $this->createConnect();
        $sql = "DELETE FROM $table WHERE $where";
        return mysqli_query($this->_conn, $sql);
    }

    // SQL to update a record
    function update($table, $data, $where) {
        $this->createConnect();
        $sql = '';
        foreach ($data as $key => $value) {
            $sql .= "$key = '" . mysql_escape_string($value) . "',";
        }
        $sql = 'UPDATE ' . $table . ' SET ' . trim($sql, ',') . ' WHERE ' . $where;
        return mysqli_query($this->_conn, $sql);
    }

    // Close connection
    function closeConnect() {
        return mysqli_close($this->_conn);
    }

    // Get list data
    function getList($sql) {
        $this->createConnect();
        $result = mysqli_query($this->_conn, $sql);
        if (!$result) {
            die('Query invalid');
        }
        $return = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $return[] = $row;
        }
        mysqli_free_result($result);
        return $return;
    }

    // Get row
    function getRow($sql) {
        $this->createConnect();
        $result = mysqli_query($this->_conn, $sql);
        if (!$result) {
            die('Query invalid');
        }
        $row = mysqli_fetch_assoc($result);
        mysqli_free_result($result);
        if ($row) {
            return $row;
        }
    }

}

?>