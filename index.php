<?php
session_start();
include("inc/header.php");
require_once("Model/User.php");
$user = new User();
$alert = "";
if (isset($_POST["txt-loginusername"]) && isset($_POST["txt-loginpassword"])) {
    $inputUser = $_POST["txt-loginusername"];
    $inputPass = $_POST["txt-loginpassword"];
    $name = $user->checkLogin($inputUser, $inputPass);
    if ($name !== null) {
        $_SESSION["name"] = $name;
        $alert = "";
        header('Location: main.php');
    } else {
        $alert = "Cannot access!";
    }
}
?>
<div id="main" class="container">
    <h2>Login</h2>
    <div class="login">
        <form id="login-form" action="" method="post">
            <div class="login-input">
                <input type="text" class="txt-username" id="txt-loginusername" name="txt-loginusername" placeholder="Username" 
                       value="<?php echo isset($_POST['txt-loginusername'])?$_POST['txt-loginusername']:''; ?>"/>
                <p id="alert-loginusername"></p>
            </div>
            <div class="login-input">
                <input type="password" class="txt-password" id="txt-loginpassword" name="txt-loginpassword" placeholder="Password" 
                       value="<?php echo isset($_POST['txt-loginpassword'])?$_POST['txt-loginpassword']:''; ?>"/>
                <p id="alert-loginpassword"></p>
            </div>
            <div class="login-input">
                <input type="submit" id="btn-login" value="Login"/>
                <p id="alert-login"><?php echo $alert; ?></p>
            </div>
        </form>
    </div>
</div>
</body>
</html>
