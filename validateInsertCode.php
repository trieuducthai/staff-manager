<?php

require_once("Model/User.php");
$code = $_POST['code'];
$user = new User();
if ($user->checkCode($code)) {

    echo json_encode(array('status' => 1));
} else {
    echo json_encode(array('status' => 0));
}
?>