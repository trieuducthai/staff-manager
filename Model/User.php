<?php

require("Lib/DataProvider.php");
require("Entity/Staff.php");
class User extends DataProvider {

    protected $_table = 'tbl_staff_info';
    protected $_code = 'code';
    protected $_user = 'username';
    protected $_pass = 'password';
    protected $_firstname = 'firstname';
    protected $_lastname = 'lastname';
    protected $_gender = 'gender';
    protected $_birthday = 'birthday';
    protected $_phone = 'phone';
    protected $_address = 'address';
    protected $_division = 'division';
    const GENDER_MALE = 'Male';
    const GENDER_FEMALE = 'Female';
    const GENDER_OTHER = 'Other';

    function User() {
        
    }

    public function insertUser(Staff $staff) {
        try {
            $arr = array(
                $this->_code => $staff->getCode(),
                $this->_user => $staff->getUsername(),
                $this->_pass => $staff->getPassword(),
                $this->_firstname => $staff->getFirstname(),
                $this->_lastname => $staff->getLastname(),
                $this->_gender => $staff->getGender(),
                $this->_birthday => $staff->getBirthday(),
                $this->_phone => $staff->getPhone(),
                $this->_address => $staff->getAddress(),
                $this->_division => $staff->getDivision()
            );
            return $this->insert($this->_table, $arr);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function deleteUser($code) {
        return $this->delete($this->_table, $this->_code . "='" . $code . "'");
    }

    public function updateUser(Staff $staff, $code) {
        try {
            $arr = array(
                $this->_code => $staff->getCode(),
                $this->_user => $staff->getUsername(),
                $this->_pass => $staff->getPassword(),
                $this->_firstname => $staff->getFirstname(),
                $this->_lastname => $staff->getLastname(),
                $this->_gender => $staff->getGender(),
                $this->_birthday => $staff->getBirthday(),
                $this->_phone => $staff->getPhone(),
                $this->_address => $staff->getAddress(),
                $this->_division => $staff->getDivision()
            );
            return $this->update($this->_table, $arr, $this->_code . "='" . $code . "'");
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function checkLogin($username, $password) {
        $array = $this->getList('SELECT ' . $this->_user . ', ' . $this->_pass . ', ' . $this->_firstname . ' FROM ' . $this->_table
        . ' WHERE ' . $this->_user . "='" . $username . "'");
        $result = $this->getData($array);
        
        if (!$result){
            return null;
        }
        
        $user = $result[0];
        
        if (password_verify($password, $user->getPassword())){
            return $user->getFirstname();
        }
        else {
            return null;
        }
    }
    
    public function getCode() {
        $arr = $this->getList('SELECT '. $this->_code .' FROM ' . $this->_table);
        return $this->getData($arr);
    }
    
    public function checkCode($code) {
        $arr = $this->getList('SELECT '. $this->_code .' FROM ' . $this->_table . ' WHERE ' . $this->_code . "='" . $code . "'");
        if (count($arr) === 0) {
            return true;
        }
        else {
            return false;
        }
    }

    public function getPager($start, $limit, $keyword, $value) {
        $sql = "SELECT * FROM " . $this->_table;
        $i = 0;
        if (count($keyword) > 0){
             $sql.= " WHERE ";
        }
        while ($i < count($keyword)) {
            $sql.= $keyword[$i] . " LIKE '%" . $value . "%'";
            if ($i < count($keyword) - 1) {
                $sql.= ' OR ';
            }
            $i++;
        }
        $sql.= ' ORDER BY ' . $this->_code . ' LIMIT ' . $start . ',' . $limit;
        $arr = $this->getList($sql);
        return $this->getData($arr);
    }

    public function getAllData() {
        $arr = $this->getList('SELECT * FROM ' . $this->_table . ' ORDER BY ' . $this->_code);
        return $this->getData($arr);
    }

    public function getSearch($keyword, $value) {
        $sql = "SELECT * FROM " . $this->_table;
        $i = 0;
        if (count($keyword) > 0){
             $sql.= " WHERE ";
        }
        while ($i < count($keyword)) {
            $sql.= $keyword[$i] . " LIKE '" . $value . "%'";
            if ($i < count($keyword) - 1) {
                $sql.=' OR ';
            }
            $i++;
        }
        $arr = $this->getList($sql);
        return $this->getData($arr);
    }

    private function getData($arr) {
        $i = 0;
        $return = array();
        while ($i < count($arr)) {
            $staff = new Staff();
            foreach ($arr[$i] as $key => $value) {
                switch ($key) {
                    case $this->_code:$staff->setCode($value);
                    case $this->_user:$staff->setUsername($value);
                    case $this->_pass:$staff->setPassword($value);
                    case $this->_firstname:$staff->setFirstname($value);
                    case $this->_lastname:$staff->setLastname($value);
                    case $this->_gender: {
                        switch ($value) {
                            case 0:$staff->setGender(self::GENDER_FEMALE);
                                break;
                            case 1:$staff->setGender(self::GENDER_MALE);
                                break;
                            default:$staff->setGender(self::GENDER_OTHER);
                                break;
                        }
                    }
                    case $this->_birthday:$staff->setBirthday($value);
                    case $this->_phone:$staff->setPhone($value);
                    case $this->_address:$staff->setAddress($value);
                    case $this->_division:$staff->setDivision($value);
                }
            }
            $return[$i] = $staff;
            $i++;
        }
        return $return;
    }

    public function getRowbyCode($code) {
        $arr = $this->getRow('SELECT * FROM ' . $this->_table . ' WHERE ' . $this->_code . "='" . $code . "'");
        $staff = new Staff();
        foreach ($arr as $key => $value) {
            switch ($key) {
                case $this->_code:$staff->setCode($value);
                case $this->_user:$staff->setUsername($value);
                case $this->_pass:$staff->setPassword($value);
                case $this->_firstname:$staff->setFirstname($value);
                case $this->_lastname:$staff->setLastname($value);
                case $this->_gender: {
                    switch ($value) {
                        case 0:$staff->setGender(self::GENDER_FEMALE);
                            break;
                        case 1:$staff->setGender(self::GENDER_MALE);
                            break;
                        default:$staff->setGender(self::GENDER_OTHER);
                            break;
                    }
                }
                case $this->_birthday:$staff->setBirthday($value);
                case $this->_phone:$staff->setPhone($value);
                case $this->_address:$staff->setAddress($value);
                case $this->_division:$staff->setDivision($value);
            }
        }
        return $staff;
    }

}

?>