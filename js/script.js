$(document).ready(function () {

    $("#txt-newcode").blur(function () {
        validateInsertCode($(this), $("#alert-newcode"));
    });
    $("#txt-editcode").blur(function () {
        validateUpdateCode($(this), $("#alert-editcode"));
    });
    $("#txt-loginusername").blur(function () {
        validateUsername($(this), $("#alert-loginusername"));
    });
    $("#txt-loginpassword").blur(function () {
        validatePassword($(this), $("#alert-loginpassword"));
    });
    $("#txt-newusername").blur(function () {
        validateUsername($(this), $("#alert-newusername"));
    });
    $("#txt-newpassword").blur(function () {
        validatePassword($(this), $("#alert-newpassword"));
    });
    $("#txt-editusername").blur(function () {
        validateUsername($(this), $("#alert-editusername"));
    });
    $("#txt-editpassword").blur(function () {
        validatePassword($(this), $("#alert-editpassword"));
    });
    $("#txt-newbirthday").blur(function () {
        validateBirthday($(this), $("#alert-newbirthday"));
    });
    $("#txt-editbirthday").blur(function () {
        validateBirthday($(this), $("#alert-editbirthday"));
    });
    $("#txt-newphone").blur(function () {
        validatePhone($(this), $("#alert-newphone"));
    });
    $("#txt-editphone").blur(function () {
        validatePhone($(this), $("#alert-editphone"));
    });
    $("#log-out").click(function () {
        if (confirm("Are you sure you want to logout?")) {
            $.ajax({url: "closeSesson.php", success: function () {
                    $(".user-login").hide();
                    window.location.href = "index.php";
                }});
        } else {
            return false;
        }
    });
    $(".delete").click(function () {
        var deleteCode = $(this).attr('data-code');
        $.post("delete.php", {code: deleteCode}, function (result) {
            if (result.status === 1) {
                location.reload();
            } else {
                alert(result.message);
            }
        }, 'json');
    });
    $(".edit").click(function () {
        window.userCode = $(this).attr('data-code');
        $.post("getUser.php", {usercode: window.userCode}, function (value) {
            $("#txt-editcode").val(value.code);
            $("#txt-editusername").val(value.username);
            $("#txt-editpassword").val(value.password);
            $("#txt-editfirstname").val(value.firstname);
            $("#txt-editlastname").val(value.lastname);
            switch (value.gender) {
                case 'Female':
                    $("#rdo-editfemale").prop("checked", true);
                    break;
                case 'Male':
                    $("#rdo-editmale").prop("checked", true);
                    break;
                default:
                    $("#rdo-editother").prop("checked", true);
                    break;
            }
            $("#txt-editbirthday").val(value.birthday);
            $("#txt-editphone").val(value.phone);
            $("#txt-editaddress").val(value.address);
            $("#txt-editdivision").val(value.division);

        }, 'json');
        $("#update-main").fadeToggle("fast");
    });

    $("#insert").click(function () {
        $("#txt-newcode").val('');
        $("#txt-newusername").val('');
        $("#txt-newpassword").val('');
        $("#txt-newfirstname").val('');
        $("#txt-newlastname").val('');
        $("#txt-newbirthday").val('');
        $("#txt-newphone").val('');
        $("#txt-newaddress").val('');
        $("#txt-newdivision").val('');
        $("#addnew-main").fadeToggle("fast");
    });

    $("#btn-addnew").click(function () {

        var valCode = validateInsertCode($("#txt-newcode"), $("#alert-newcode"));
        var valUser = validateUsername($("#txt-newusername"), $("#alert-newusername"));
        var valPass = validatePassword($("#txt-newpassword"), $("#alert-newpassword"));
        var valBirth = validateBirthday($("#txt-newbirthday"), $("#alert-newbirthday"));
        var valPhone = validatePhone($("#txt-newphone"), $("#alert-newphone"));
        if (valCode && valUser && valPass && valBirth && valPhone) {
            var code = $("#txt-newcode").val();
            var username = $("#txt-newusername").val();
            var password = $("#txt-newpassword").val();
            var firstname = $("#txt-newfirstname").val();
            var lastname = $("#txt-newlastname").val();
            var gender = $("#add-form input[type='radio']:checked").val();
            var birthday = $("#txt-newbirthday").val();
            var phone = $("#txt-newphone").val();
            var address = $("#txt-newaddress").val();
            var division = $("#txt-newdivision").val();
            $.post("addnew.php", {code: code, username: username, password: password,
                firstname: firstname, lastname: lastname, gender: gender, birthday: birthday,
                phone: phone, address: address, division: division}, function (result) {
                if (result.status === 1) {
                    window.location.reload();
                } else {
                    alert(result.message);
                }
            }, 'json');
        }
    });
    
     $("#btn-update").click(function () {
        var valCode = validateUpdateCode($("#txt-editcode"), $("#alert-editcode"));
        var valUser = validateUsername($("#txt-editusername"), $("#alert-editusername"));
        var valPass = validatePassword($("#txt-editpassword"), $("#alert-editpassword"));
        var valBirth = validateBirthday($("#txt-editbirthday"), $("#alert-editbirthday"));
        var valPhone = validatePhone($("#txt-editphone"), $("#alert-editphone"));
        if (valCode && valUser && valPass && valBirth && valPhone) {
            var code = $("#txt-editcode").val();
            var username = $("#txt-editusername").val();
            var password = $("#txt-editpassword").val();
            var firstname = $("#txt-editfirstname").val();
            var lastname = $("#txt-editlastname").val();
            var gender = $("#update-form input[type='radio']:checked").val();
            var birthday = $("#txt-editbirthday").val();
            var phone = $("#txt-editphone").val();
            var address = $("#txt-editaddress").val();
            var division = $("#txt-editdivision").val();
            $.post("update.php", {oldcode: window.userCode, code: code, username: username, password: password,
                firstname: firstname, lastname: lastname, gender: gender, birthday: birthday,
                phone: phone, address: address, division: division}, function (result) {
                if (result.status === 1) {
                    window.location.reload();
                } else {
                    alert(result.message);
                }
            }, 'json');
        }
    });

    $(".addnew-close").click(function () {
        $("#addnew-main").fadeToggle("fast");
    });

    $(".update-close").click(function () {
        $("#update-main").fadeToggle("fast");
    });
});

function validateInsertCode(textEle, alertEle) {
    alertEle.css("color", "#bd3200");
    var code = textEle.val();
    var reg = /^[\w\.-]{3,16}$/;
    var checkInsert;
    if (reg.test(code)) {

        $.post("validateInsertCode.php", {code: code}, function (result) {
            if (result.status === 1) {
                alertEle.text("");
                textEle.css("outline", "initial");
                window.checkIn = true;
            } else {
                alertEle.text("Staff code invalid");
                textEle.css("outline", "1px solid #bd3200");
                window.checkIn = false;
            }
        }, 'json');
        checkInsert = window.checkIn;
    } else {
        if (code === "") {
            alertEle.text("Please input staff code.");
            textEle.css("outline", "1px solid #bd3200");
        } else if (code.length < 3) {
            alertEle.text("Staff code is too short.");
            textEle.css("outline", "1px solid #bd3200");
        } else {
            alertEle.text("Staff code invalid");
            textEle.css("outline", "1px solid #bd3200");
        }
        checkInsert = false;
    }
    return checkInsert;
}

function validateUpdateCode(textEle, alertEle) {
    alertEle.css("color", "#bd3200");
    var code = textEle.val();
    var reg = /^[\w\.-]{3,16}$/;
    var checkUpdate;
    if (reg.test(code)) {

        $.post("validateUpdateCode.php", {oldcode:window.userCode, code: code}, function (result) {
            if (result.status === 1) {
                alertEle.text("");
                textEle.css("outline", "initial");
                window.checkUp = true;
            } else {
                alertEle.text("Staff code invalid");
                textEle.css("outline", "1px solid #bd3200");
                window.checkUp = false;
            }
        }, 'json');
        checkUpdate = window.checkUp;
    } else {
        if (code === "") {
            alertEle.text("Please input staff code.");
            textEle.css("outline", "1px solid #bd3200");
        } else if (code.length < 3) {
            alertEle.text("Staff code is too short.");
            textEle.css("outline", "1px solid #bd3200");
        } else {
            alertEle.text("Staff code invalid");
            textEle.css("outline", "1px solid #bd3200");
        }
        checkUpdate = false;
    }
    return checkUpdate;
}

function validateUsername(textEle, alertEle) {
    alertEle.css("color", "#bd3200");
    var check;
    var user = textEle.val();
    var reg = /^[\w\.-]{3,16}$/;
    if (reg.test(user)) {
        alertEle.text("");
        textEle.css("outline", "initial");
        check = true;
    } else {
        if (user === "") {
            alertEle.text("Please input your username.");
            textEle.css("outline", "1px solid #bd3200");
        } else if (user.length < 3) {
            alertEle.text("Your username is too short.");
            textEle.css("outline", "1px solid #bd3200");
        } else {
            alertEle.text("Username invalid");
            textEle.css("outline", "1px solid #bd3200");
        }
        check = false;
    }
    return check;
}

function validatePassword(textEle, alertEle) {
    alertEle.css("color", "#bd3200");
    var check;
    var pass = textEle.val();
    var reg = /\S{6,16}/;
    if (reg.test(pass)) {
        alertEle.text("");
        textEle.css("outline", "initial");
        check = true;
    } else {
        if (pass === "") {
            alertEle.text("Please input your password.");
            textEle.css("outline", "1px solid #bd3200");
        } else if (pass.length < 6) {
            alertEle.text("Your password is too short.");
            textEle.css("outline", "1px solid #bd3200");
        } else {
            alertEle.text("Password invalid");
            textEle.css("outline", "1px solid #bd3200");
        }
        check = false;
    }
    return check;
}

function validateBirthday(textEle, alertEle) {
    alertEle.css("color", "#bd3200");
    var check;
    var birthday = textEle.val();
    var reg = /^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/;
    if (reg.test(birthday)) {
        alertEle.text("");
        textEle.css("outline", "initial");
        check = true;
    } else {
        if (birthday === "") {
            alertEle.text("Please input your birthday.");
            textEle.css("outline", "1px solid #bd3200");
        } else {
            alertEle.text("Birthday invalid");
            textEle.css("outline", "1px solid #bd3200");
        }
        check = false;
    }
    return check;
}

function validatePhone(textEle, alertEle) {
    alertEle.css("color", "#bd3200");
    var check;
    var phone = textEle.val();
    var reg = /\+?[\d.]{1,3}\-?\(?[\d.]{2,4}\)?\-?[\d.]{2,4}\-?[\d]{2,6}/;
    if (reg.test(phone)) {
        alertEle.text("");
        textEle.css("outline", "initial");
        check = true;
    } else {
        if (phone === "") {
            alertEle.text("Please input your phone.");
            textEle.css("outline", "1px solid #bd3200");
        } else {
            alertEle.text("Phone invalid");
            textEle.css("outline", "1px solid #bd3200");
        }
        check = false;
    }
    return check;
}